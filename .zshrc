#Pacman hook
TRAPUSR1() { 
	[[ $(uname -r) == $(file /boot/vmlinuz-linux | cut -d" " -f9) ]] && reboot="" || reboot="↺ "
	PROMPT="%B%{$fg[red]%}$reboot%{$fg[cyan]%}%~%b %B%(?.%{$fg[cyan]%}.%{$fg[red]%})➜%{$reset_color%}%b "
	rehash
}

# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

#Plugins
plugs=(
/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
/usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
)

for p in $plugs; do
	[ -f $p ] && source $p
done

autoload -Uz compinit colors vcs_info
compinit -d
colors

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # automatically find new executables in path 
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/.zsh

#setopt
setopt AUTO_CD
setopt PROMPT_PERCENT
setopt PROMPT_SUBST
setopt ALWAYS_TO_END
setopt BANG_HIST
setopt EXTENDED_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_VERIFY
setopt CORRECT
setopt INTERACTIVE_COMMENTS
setopt EMACS

## Keybindings section
bindkey '^[[7~' beginning-of-line                               # Home key
bindkey '^[[H' beginning-of-line                                # Home key
if [[ "${terminfo[khome]}" != "" ]]; then
  bindkey "${terminfo[khome]}" beginning-of-line                # [Home] - Go to beginning of line
fi
bindkey '^[[8~' end-of-line                                     # End key
bindkey '^[[F' end-of-line                                     # End key
if [[ "${terminfo[kend]}" != "" ]]; then
  bindkey "${terminfo[kend]}" end-of-line                       # [End] - Go to end of line
fi
bindkey '^[[2~' overwrite-mode                                  # Insert key
bindkey '^[[3~' delete-char                                     # Delete key
bindkey '^[[C'  forward-char                                    # Right key
bindkey '^[[D'  backward-char                                   # Left key
bindkey '^[[5~' history-beginning-search-backward               # Page up key
bindkey '^[[6~' history-beginning-search-forward                # Page down key

# Navigate words with ctrl+arrow keys
bindkey '^[Oc' forward-word                                     #
bindkey '^[Od' backward-word                                    #
bindkey '^[[1;5D' backward-word                                 #
bindkey '^[[1;5C' forward-word                                  #
bindkey '^H' backward-kill-word                                 # delete previous word with ctrl+backspace
bindkey '^[[Z' undo                                             # Shift+tab undo last action

# bind UP and DOWN arrow keys to history substring search
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

#Greeting
GREEN="\033[1;32m"
RED="\033[1;31m"
NOCOLOR="\033[0m"
greet="$USER@$HOST  $(uname -srm) $(lsb_release -ds)"
[[ $(uname -r) == $(file /boot/vmlinuz-linux | cut -d" " -f9) ]] && reboot="" || reboot="↺ "
echo -ne "${GREEN}$greet${NOCOLOR}"
[[ -z $reboot ]] && echo || echo -e " $reboot${RED}$(pacman -Q linux) installed${NOCOLOR}"

#PROMPT: robbyrussel inverted
PROMPT="%B%{$fg[red]%}$reboot%{$fg[cyan]%}%~%b %B%(?.%{$fg[cyan]%}.%{$fg[red]%})➜%{$reset_color%}%b "
RPROMPT="%{$fg[red]%} %(?..[%?])"

##Exports
# Color man pages
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r

# GNU and BSD (macOS) ls flags aren't compatible
ls --version &>/dev/null
if [ $? -eq 0 ]; then
  lsflags="--color --group-directories-first -F"
else
  lsflags="-GF"
  export CLICOLOR=1
fi

# Aliases
alias ls="ls ${lsflags}"
alias ll="ls ${lsflags} -l"
alias la="ls ${lsflags} -la"
